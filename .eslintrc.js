module.exports = {
  root: true,

  env: {
    node: true
  },

  "extends": [
    "plugin:vue/essential",
    "eslint:recommended",
    "@vue/typescript/recommended"
  ],

  parserOptions: {
    ecmaVersion: 2020
  },

  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "semi": "error",
    "quotes": ["error", "double"],
    "vue/html-indent": ["error", 2],
    "indent": ["error", 2, {
      "SwitchCase": 1
    }],
    "vue/script-indent": ["error", 2, {
      "switchCase": 1
    }]
  },

  overrides: [
    {
      files: [
        "**/__tests__/*.{j,t}s?(x)",
        "**/tests/unit/**/*.spec.{j,t}s?(x)"
      ],
      env: {
        jest: true
      }
    },
    {
      "files": ["*.vue"],
      "rules": {
        "indent": "off"
      }
    }
  ]
};
