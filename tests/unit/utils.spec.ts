import { parseSubscriptionContent } from "@/shared/utils";
import { emptySSRNode, toSSRLink, sameConfig, SSRNode } from "@/shared/Nodes";
import { Base64 } from "js-base64";
describe("parseSubscriptionContent", () => {
  it("SSR Subscription Content", () => {
    const generatedNodeArr: Array<SSRNode> = [];
    for (let index = 0; index < 10; index++) {
      const node = emptySSRNode();
      node.server = `${index}.example.com`;
      node.method = "aes-256-cfb";
      node.obfs = "plain";
      node.group = "Group X";
      node.remarks = `Test ${index}`;
      generatedNodeArr.push(node);
    }
    const encodedContent = Base64.encodeURI(generatedNodeArr.map((node) => toSSRLink(node)).join("\n"));
    const parsedList = parseSubscriptionContent(encodedContent);
    expect(parsedList.length).toBe(generatedNodeArr.length);
    parsedList.forEach((node, idx) => {
      expect(sameConfig(node, generatedNodeArr[idx])).toBe(true);
      expect(node.group === generatedNodeArr[idx].group).toBe(true);
      expect(node.remarks === generatedNodeArr[idx].remarks).toBe(true);
    });
  });
  it("SSR from https://git.io/autossr_stable", () => {
    // Only for parsing test.
    const rawCotnent = "c3NyOi8vTVRreUxqSTBNQzQ1T1M0eE9ESTZPREE1TnpwdmNtbG5hVzQ2WVdWekxUSTFOaTFqWm1JNmNHeGhhVzQ2V2xWc1dFMUZVblZoZWxrMVRrUlZNRnBVV25WVk0yUXhZek5DTWs5VlVuUlZla2wzVFZoU1VrMUZVUzhfYjJKbWMzQmhjbUZ0UFNad2NtOTBiM0JoY21GdFBTWnlaVzFoY210elBVa3dVWGRQVlVsMlVWWk5lRTU2VVRaSlJsWjFZVmhTYkZwRFFsUmtSMFl3V2xoTkptZHliM1Z3UFZGWVZqQmlNVTVVVldjCnNzcjovL05EVXVNVFF3TGpFM01DNHhOVFE2TWpBd05qZzZiM0pwWjJsdU9tRmxjeTB5TlRZdFkyWmlPbkJzWVdsdU9sUldOVlpWYlZwNVkxVXhTVmt3WkdsU01WVjVVVWM0TVZKc1FsQlBWVXBGVGtWT1VWTkdWbXBhTWxVdlAyOWlabk53WVhKaGJUMG1jSEp2ZEc5d1lYSmhiVDBtY21WdFlYSnJjejFKZWtVeFVUQlpkbEZXVFRCUFZFMDFUV3B2WjFWdVZucGpNbXhvSm1keWIzVndQVkZZVmpCaU1VNVVWV2MKc3NyOi8vTVRNNUxqRTJNaTR4TlM0ME5UbzRNRGs1T205eWFXZHBianBoWlhNdE1qVTJMV05tWWpwd2JHRnBianBhVld4WVRVVlNkV0Y2V1RWT1JGVXdXbFJhZFZVelpERmpNMEl5VDFWU2RGVjZTWGROV0ZKU1RVVlJMejl2WW1aemNHRnlZVzA5Sm5CeWIzUnZjR0Z5WVcwOUpuSmxiV0Z5YTNNOVNYcGtSMDFWVVhaUlZrMHlUWHByTUU5VWIyZFZNbXgxV2pKR2QySXpTbXdtWjNKdmRYQTlVVmhXTUdJeFRsUlZadwpzc3I6Ly9NVGs1TGpJeU15NHhNVGt1T1RJNk9EQTVPVHB2Y21sbmFXNDZZV1Z6TFRJMU5pMWpabUk2Y0d4aGFXNDZXbFZzV0UxRlVuVmhlbGsxVGtSVk1GcFVXblZWTTJReFl6TkNNazlWVW5SVmVrbDNUVmhTVWsxRlVTOF9iMkptYzNCaGNtRnRQU1p3Y205MGIzQmhjbUZ0UFNaeVpXMWhjbXR6UFVsNmJFZFJlazEyVVZaTk1VMTZaelJQVkc5blZsYzFjR1JIVm10SlJrNHdXVmhTYkdOM0ptZHliM1Z3UFZGWVZqQmlNVTVVVldjCnNzcjovL01UWXlMakl3T1M0eU1UZ3VNem95TkRVNU5UcHZjbWxuYVc0NmNtTTBMVzFrTlRwd2JHRnBianBOUmxrMVltcG9ORTVWUm5sbFFTOF9iMkptYzNCaGNtRnRQU1p3Y205MGIzQmhjbUZ0UFNaeVpXMWhjbXR6UFVrd1ZYZE9hbXQyVVZaTk1FMUVRVEpPVkc5blZsYzFjR1JIVm10SlJrNHdXVmhTYkdOM0ptZHliM1Z3UFZGWVZqQmlNVTVVVldjCnNzcjovL01UY3lMakV3TlM0eE9USXVNVE0xT2pnd09UazZiM0pwWjJsdU9tRmxjeTB5TlRZdFkyWmlPbkJzWVdsdU9scFZiRmhOUlZKMVlYcFpOVTVFVlRCYVZGcDFWVE5rTVdNelFqSlBWVkowVlhwSmQwMVlVbEpOUlZFdlAyOWlabk53WVhKaGJUMG1jSEp2ZEc5d1lYSmhiVDBtY21WdFlYSnJjejFKZWtwQ1RucHJkbEZXVFRKTmVtc3dUMVJ2WjFOdFJuZFpWelFtWjNKdmRYQTlVVmhXTUdJeFRsUlZadwpzc3I6Ly9Oall1TWpJNExqUTVMakl6TmpveU1UTTRNVHB2Y21sbmFXNDZZV1Z6TFRJMU5pMWpabUk2Y0d4aGFXNDZVMFJLVkdSdWNEUldNMEUxV1c1YVF5OF9iMkptYzNCaGNtRnRQU1p3Y205MGIzQmhjbUZ0UFNaeVpXMWhjbXR6UFVsNlFUQlJlbGwyVVZaTk1rMTZhekJQVkc5blZsYzFjR1JIVm10SlJrNHdXVmhTYkdOM0ptZHliM1Z3UFZGWVZqQmlNVTVVVldj";
    const parsedList = parseSubscriptionContent(rawCotnent);
    parsedList.forEach((node) => {
      expect(node.group).toBe("AutoSSR");
    });
    expect(parsedList.length).toBe(7);
  });
});