/* eslint-disable @typescript-eslint/camelcase */
import { isSSRNode, emptySSRNode, sameConfig, isSSNode, emptySSNode, toSSNode, toSSRNode, splitSSRLink, parseSSRLink, splitSSLink, parseSSLink, toSSRLink } from "@/shared/Nodes";
describe("isSSRNode", () => {
  it("EmptyObject", () => {
    const emptyNode = {};
    expect(isSSRNode(emptyNode)).toBe(false);
  });
  it("Partial", () => {
    const partialNode = {
      protocol: "777",
      obfs: "111"
    };
    expect(isSSRNode(partialNode)).toBe(false);
  });
  it("Generated with emptySSRNode", () => {
    const generatedNode = emptySSRNode();
    expect(isSSRNode(generatedNode)).toBe(true);
  });
  it("null", () => {
    expect(isSSRNode(null)).toBe(false);
  });
  it("undefined", () => {
    expect(isSSRNode(undefined)).toBe(false);
  });
});
describe("isSSNode", () => {
  it("EmptyObject", () => {
    const emptyNode = {};
    expect(isSSNode(emptyNode)).toBe(false);
  });
  it("Partial", () => {
    const partialNode = {
      method: "777",
      password: "111"
    };
    expect(isSSNode(partialNode)).toBe(false);
  });
  it("Generated with emptySSNode", () => {
    const generatedNode = emptySSNode();
    expect(isSSNode(generatedNode)).toBe(true);
  });
  it("null", () => {
    expect(isSSNode(null)).toBe(false);
  });
  it("undefined", () => {
    expect(isSSNode(undefined)).toBe(false);
  });
  it("SSRNode", () => {
    const generatedNode = emptySSRNode();
    expect(isSSNode(generatedNode)).toBe(false);
  });
});
describe("sameConfig", () => {
  it("Change ID", () => {
    const generatedNode = emptySSRNode();
    const generatedNode2 = emptySSRNode();
    generatedNode2.id = "";
    expect(sameConfig(generatedNode, generatedNode2)).toBe(true);
  });
  it("Change obfs", () => {
    const generatedNode = emptySSRNode();
    const generatedNode2 = emptySSRNode();
    generatedNode2.obfs = "7777";
    expect(sameConfig(generatedNode, generatedNode2)).toBe(false);
  });

  it("One is Not SSR", () => {
    const generatedNode = emptySSRNode();
    const generatedNode2 = {};
    expect(sameConfig(generatedNode, generatedNode2)).toBe(false);
  });

  it("Both are Not SSR/SS", () => {
    const generatedNode = {};
    const generatedNode2 = {};
    expect(sameConfig(generatedNode, generatedNode2)).toBe(true);
  });

  it("Same SS", () => {
    const generatedNode = emptySSNode();
    const generatedNode2 = emptySSNode();
    expect(sameConfig(generatedNode, generatedNode2)).toBe(true);
  });
  it("Both SS, one miss plugin", () => {
    const generatedNode = emptySSNode();
    const generatedNode2 = emptySSNode();
    delete generatedNode2.plugin;
    expect(sameConfig(generatedNode, generatedNode2)).toBe(false);
  });
  it("Both SS, plugin missmatch", () => {
    const generatedNode = emptySSNode();
    const generatedNode2 = emptySSNode();
    generatedNode2.plugin = "11111";
    expect(sameConfig(generatedNode, generatedNode2)).toBe(false);
  });
  it("SSR/SS", () => {
    const generatedNode = emptySSNode();
    const generatedNode2 = emptySSRNode();
    expect(sameConfig(generatedNode, generatedNode2)).toBe(false);
  });
});

describe("toSSNode", () => {
  it("Same ID", () => {
    const generatedNode = emptySSRNode();
    expect(toSSNode(generatedNode).id).toBe(generatedNode.id);
  });
  it("Same Group", () => {
    const generatedNode = emptySSRNode();
    generatedNode.group = "Group 111";
    expect(toSSNode(generatedNode).group).toBe(generatedNode.group);
  });
  it("Same Remarks", () => {
    const generatedNode = emptySSRNode();
    generatedNode.remarks = "7777";
    expect(toSSNode(generatedNode).remarks).toBe(generatedNode.remarks);
  });

  it("Same Server", () => {
    const generatedNode = emptySSRNode();
    generatedNode.server = "7777.com";
    expect(toSSNode(generatedNode).server).toBe(generatedNode.server);
  });
  it("Same Port", () => {
    const generatedNode = emptySSRNode();
    generatedNode.server_port = 111;
    expect(toSSNode(generatedNode).server_port).toBe(generatedNode.server_port);
  });
  it("Same Method", () => {
    const generatedNode = emptySSRNode();
    generatedNode.method = "method111";
    expect(toSSNode(generatedNode).method).toBe(generatedNode.method);
  });
});

describe("toSSRNode", () => {
  it("Same ID", () => {
    const generatedNode = emptySSNode();
    expect(toSSRNode(generatedNode).id).toBe(generatedNode.id);
  });
  it("Same Group", () => {
    const generatedNode = emptySSNode();
    generatedNode.group = "Group 111";
    expect(toSSRNode(generatedNode).group).toBe(generatedNode.group);
  });
  it("Same Remarks", () => {
    const generatedNode = emptySSNode();
    generatedNode.remarks = "7777";
    expect(toSSRNode(generatedNode).remarks).toBe(generatedNode.remarks);
  });

  it("Same Server", () => {
    const generatedNode = emptySSNode();
    generatedNode.server = "7777.com";
    expect(toSSRNode(generatedNode).server).toBe(generatedNode.server);
  });
  it("Same Port", () => {
    const generatedNode = emptySSNode();
    generatedNode.server_port = 111;
    expect(toSSRNode(generatedNode).server_port).toBe(generatedNode.server_port);
  });
  it("Same Method", () => {
    const generatedNode = emptySSNode();
    generatedNode.method = "method111";
    expect(toSSRNode(generatedNode).method).toBe(generatedNode.method);
  });
});
const exampleLink1 = "ssr://ZXhhbXBsZS5jb206Nzc3NzphdXRoX2NoYWluX2I6YWVzLTI1Ni1jZmI6cGxhaW46Y1hkUmJUQkMvP29iZnNwYXJhbT1Nak16TXk1bGVHRnRjR3hsTG1OdmJRJnByb3RvcGFyYW09TmpVeE1USTZNMEZhZW5kMSZyZW1hcmtzPVZHVnpkQ0JPYjJSbElERSZncm91cD1SM0p2ZFhBeA";
const exampleLink2 = "ssr://MTI3LjAuMC4xOjEyMzQ6YXV0aF9hZXMxMjhfbWQ1OmFlcy0xMjgtY2ZiOnRsczEuMl90aWNrZXRfYXV0aDpZV0ZoWW1KaS8_b2Jmc3BhcmFtPVluSmxZV3QzWVRFeExtMXZaUSZyZW1hcmtzPTVyV0w2Sy1WNUxpdDVwYUg";
describe("splitSSRLink", () => {

  it("Empty String", () => {
    const emptyLink = "";
    expect(splitSSRLink(emptyLink)).toBe(null);
  });
  // 
  it("Example 1", () => {
    const result = splitSSRLink(exampleLink1);
    expect(result != null).toBe(true);
    expect(result?.serverInfo.join(" ")).toBe([
      "example.com",
      "7777",
      "auth_chain_b",
      "aes-256-cfb",
      "plain",
      "cXdRbTBC"
    ].join(" "));
    expect(result?.options).toStrictEqual({
      "group": "R3JvdXAx",
      "obfsparam": "MjMzMy5leGFtcGxlLmNvbQ",
      "protoparam": "NjUxMTI6M0Faend1",
      "remarks": "VGVzdCBOb2RlIDE",
    });
  });
});

describe("parseSSRLink", () => {
  it("Example 1", () => {
    const parsedNode = parseSSRLink(exampleLink1);
    expect(parsedNode == null).toBe(false);
    expect(parsedNode?.server).toBe("example.com");
    expect(parsedNode?.server_port).toBe(7777);
    expect(parsedNode?.protocol).toBe("auth_chain_b");
    expect(parsedNode?.method).toBe("aes-256-cfb");
    expect(parsedNode?.obfs).toBe("plain");
    expect(parsedNode?.password).toBe("qwQm0B");

    expect(parsedNode?.obfsparam).toBe("2333.example.com");
    expect(parsedNode?.protocolparam).toBe("65112:3AZzwu");
    expect(parsedNode?.remarks).toBe("Test Node 1");
    expect(parsedNode?.group).toBe("Group1");
  });
  it("Example 2 C#", () => {
    // https://github.com/HMBSbige/ShadowsocksR-Windows/wiki/SSR-QRcode-scheme
    const parsedNode = parseSSRLink(exampleLink2);
    expect(parsedNode == null).toBe(false);
    expect(parsedNode?.server).toBe("127.0.0.1");
    expect(parsedNode?.server_port).toBe(1234);
    expect(parsedNode?.protocol).toBe("auth_aes128_md5");
    expect(parsedNode?.method).toBe("aes-128-cfb");
    expect(parsedNode?.obfs).toBe("tls1.2_ticket_auth");
    expect(parsedNode?.password).toBe("aaabbb");

    expect(parsedNode?.obfsparam).toBe("breakwa11.moe");
    expect(parsedNode?.protocolparam).toBe("");
    expect(parsedNode?.remarks).toBe("测试中文");
    expect(parsedNode?.group).toBe("");
  });
});
const ssLinkWithTagBase64 = "ss://YmYtY2ZiOnRlc3RAMTkyLjE2OC4xMDAuMTo4ODg4#example-server";
const ssLinkWithoutTagBase64 = "ss://YmYtY2ZiOnRlc3RAMTkyLjE2OC4xMDAuMTo4ODg4";
const ssLinkWithoutTagPlain = "ss://bf-cfb:test@192.168.100.1:8888";
const ssLinkWithTagPlain = "ss://bf-cfb:test@gggg.example.com:8888#a%20server";
describe("splitSSLink", () => {
  it("Base64 with tag", () => {
    expect(splitSSLink(ssLinkWithTagBase64)).toStrictEqual({
      method: "bf-cfb",
      password: "test",
      server: "192.168.100.1",
      port: 8888,
      remarks: "example-server"
    });
  });

  it("Base64 without tag", () => {
    expect(splitSSLink(ssLinkWithoutTagBase64)).toStrictEqual({
      method: "bf-cfb",
      password: "test",
      server: "192.168.100.1",
      port: 8888
    });
  });
  it("Plain without tag", () => {
    expect(splitSSLink(ssLinkWithoutTagPlain)).toStrictEqual({
      method: "bf-cfb",
      password: "test",
      server: "192.168.100.1",
      port: 8888
    });
  });
  it("Plain with tag and spaced", () => {
    expect(splitSSLink(ssLinkWithTagPlain)).toStrictEqual({
      method: "bf-cfb",
      password: "test",
      server: "gggg.example.com",
      port: 8888,
      remarks: "a server"
    });
  });
});
describe("parseSSLink", () => {
  it("Base64 with tag", () => {
    const node = parseSSLink(ssLinkWithTagBase64);
    expect(node == null).toBe(false);
    expect(node?.server).toBe("192.168.100.1");
    expect(node?.server_port).toBe(8888);
    expect(node?.method).toBe("bf-cfb");
    expect(node?.password).toBe("test");
    expect(node?.remarks).toBe("example-server");
  });
  it("Plain with tag", () => {
    const node = parseSSLink(ssLinkWithTagPlain);
    expect(node == null).toBe(false);
    expect(node?.server).toBe("gggg.example.com");
    expect(node?.server_port).toBe(8888);
    expect(node?.method).toBe("bf-cfb");
    expect(node?.password).toBe("test");
    expect(node?.remarks).toBe("a server");
  });
  it("SIP002 No Plugin", () => {
    const node = parseSSLink("ss://YWVzLTEyOC1nY206dGVzdA==@192.168.100.1:8888#Example1");
    expect(node == null).toBe(false);
    expect(node?.server).toBe("192.168.100.1");
    expect(node?.server_port).toBe(8888);
    expect(node?.method).toBe("aes-128-gcm");
    expect(node?.password).toBe("test");
    expect(node?.remarks).toBe("Example1");
  });
  it("SIP002 with Plugin obfs-local", () => {
    const node = parseSSLink("ss://cmM0LW1kNTpwYXNzd2Q=@192.168.100.1:8888/?plugin=obfs-local%3Bobfs%3Dhttp#Example2");
    expect(node == null).toBe(false);
    expect(node?.server).toBe("192.168.100.1");
    expect(node?.server_port).toBe(8888);
    expect(node?.method).toBe("rc4-md5");
    expect(node?.password).toBe("passwd");
    expect(node?.remarks).toBe("Example2");
    expect(node?.plugin).toBe("obfs-local");
    expect(node?.plugin_opts).toBe("obfs=http");
  });
  it("SIP002 with Plugin obfs-local and Group", () => {
    const node = parseSSLink("ss://YWVzLTI1Ni1nY206UnBTeEIz@cnrelay02.example.com:50002/?plugin=obfs-local%3Bobfs%3Dtls%3Bobfs-host%3D7777.example.com&group=RXhhbXBsZSBHcm91cA#Test%20Node%20111");
    expect(node == null).toBe(false);
    expect(node?.server).toBe("cnrelay02.example.com");
    expect(node?.server_port).toBe(50002);
    expect(node?.method).toBe("aes-256-gcm");
    expect(node?.password).toBe("RpSxB3");
    expect(node?.remarks).toBe("Test Node 111");
    expect(node?.plugin).toBe("obfs-local");
    expect(node?.plugin_opts).toBe("obfs=tls;obfs-host=7777.example.com");
  });
  it("SIP002 with Group no plugin", () => {
    const node = parseSSLink("ss://YWVzLTEyOC1jZmI6eWR3QzRl@tw.example.com:38780/?group=RXhhbXBsZSBHcm91cA#%5BV1%5D%20%E5%8F%B0%E6%B9%BEHinet%2002");
    expect(node == null).toBe(false);
    expect(node?.server).toBe("tw.example.com");
    expect(node?.server_port).toBe(38780);
    expect(node?.method).toBe("aes-128-cfb");
    expect(node?.password).toBe("ydwC4e");
    expect(node?.remarks).toBe("[V1] 台湾Hinet 02");
    expect(node?.plugin).toBe("");
    expect(node?.plugin_opts).toBe("");
  });
});

describe("toSSRLink", () => {
  it("Example 1", () => {
    const node1 = emptySSRNode();
    delete node1.id;
    node1.server = "example.com";
    node1.server_port = 7777;
    node1.protocol = "auth_chain_b";
    node1.method = "aes-256-cfb";
    node1.obfsparam = "plain";
    node1.password = "qwQm0B";

    node1.group = "Group1";
    node1.obfsparam = "2333.example.com";
    node1.protocolparam = "65112:3AZzwu";
    node1.remarks = "Test Node 1";
    const reparsedNode = parseSSRLink(toSSRLink(node1));
    expect(reparsedNode == null).toBe(false);
    delete reparsedNode?.id;
    expect(reparsedNode).toStrictEqual(node1);
  });
  it("C# Compatibility", () => {
    // https://github.com/HMBSbige/ShadowsocksR-Windows/wiki/SSR-QRcode-scheme
    const test = "ssr://MTI3LjAuMC4xOjEyMzQ6YXV0aF9hZXMxMjhfbWQ1OmFlcy0xMjgtY2ZiOnRsczEuMl90aWNrZXRfYXV0aDpZV0ZoWW1KaS8_b2Jmc3BhcmFtPVluSmxZV3QzWVRFeExtMXZaUQ";
    const reparsedNode = parseSSRLink(test);
    if (reparsedNode == null) {
      throw "reparsedNode NULL";
    }
    expect(reparsedNode !== null).toBe(true);
    expect(toSSRLink(reparsedNode)).toBe(test);
  });

  it("One from https://git.io/autossr_stable", () => {
    const test = "ssr://MTkyLjI0MC45OS4xODI6ODA5NzpvcmlnaW46YWVzLTI1Ni1jZmI6cGxhaW46WlVsWE1FUnVhelk1TkRVMFpUWnVVM2QxYzNCMk9VUnRVekl3TVhSUk1FUS8_b2Jmc3BhcmFtPSZwcm90b3BhcmFtPSZyZW1hcmtzPUkwUXdPVUl2UVZNeE56UTZJRlZ1YVhSbFpDQlRkR0YwWlhNJmdyb3VwPVFYVjBiMU5UVWc";
    const node1 = parseSSRLink(test);
    if (node1 == null) {
      throw "reparsedNode NULL";
    }
    expect(node1 !== null).toBe(true);
    const reparsedNode = parseSSRLink(toSSRLink(node1));
    expect(sameConfig(reparsedNode, node1)).toBe(true);
    expect(reparsedNode?.group).toBe(node1.group);
    expect(reparsedNode?.remarks).toBe(node1.remarks);
  });

});