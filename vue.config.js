module.exports = {
  css: {
    loaderOptions: {
      scss: {
        prependData:"@import \"~@/render/assets/styles/global.scss\";"
      }
    }
  },
  pluginOptions: {
    electronBuilder: {
      mainProcessFile: "src/main/index.ts",
      mainProcessWatch: ["src/main/**"],
    }
    // i18n: {
    //   locale: 'en-US',
    //   fallbackLocale: 'en-US',
    //   localeDir: 'renderer/locales',
    //   enableInSFC: false
    // }
  },
  chainWebpack: (config) => {
    config.entry("app")
      .clear()
      .add("./src/render/main.ts")
      .end();
  },
  configureWebpack: {
    devtool: "source-map"
  }
};
