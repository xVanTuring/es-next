import { app } from "electron";
import path from "path";
export const appConfigDir = app.getPath("userData");
// 应用配置存储路径
export const appConfigFilePath = path.join(appConfigDir, "gui-config.json");
export const logFolder = path.join(appConfigDir, "logs");