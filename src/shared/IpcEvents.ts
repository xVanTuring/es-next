export enum IpcEvents {
    INIT_CONFIG = "INIT_CONFIG",
    SYNC_APP_CONFIG = "SYNC_APP_CONFIG"
}