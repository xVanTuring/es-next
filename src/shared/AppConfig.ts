import { BasicNode } from "./Nodes";
export enum SysProxyMode {
  None,
  Pac,
  Gloabl
}
export interface ServerSubscription {
  url: string;
  group?: string;
  lastedUpdated: number;
}
export interface AppConfig {
  configs: Array<BasicNode>;
  index: number;
  enable: boolean;
  autoLaunch: boolean;
  hideWindow: boolean;
  shareOverLan: boolean;
  localPort: number;
  pacPort: number;
  sysProxyMode: SysProxyMode;
  serverSubscribes: ServerSubscription[];
  httpProxyEnable: boolean;
  httpProxyPort: number;
  lang: "en-US";
  meta: {
    isMacToolInstalled: boolean;
    noMacToolInstall: boolean;
  };
}

export const defaultConfig: AppConfig = {
  configs: [],
  index: -1,
  enable: false,
  autoLaunch: false,
  hideWindow: false,
  shareOverLan: false,
  localPort: 1080,
  pacPort: 2333,
  sysProxyMode: SysProxyMode.None,
  serverSubscribes: [],
  httpProxyEnable: false,
  httpProxyPort: 12333,
  lang: "en-US",
  meta: {
    isMacToolInstalled: false,
    noMacToolInstall: false
  }
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function mergeAppConfig(appConfig: any) {
  (Object.keys(defaultConfig) as Array<keyof AppConfig>).forEach(key => {
    const defaultValue = defaultConfig[key];
    if (appConfig[key] === undefined || typeof appConfig[key] !== typeof defaultValue) {
      // replace old obj
      appConfig[key] = defaultValue;
    } else if (typeof defaultValue === "object" && !Array.isArray(defaultValue)) {
      // both are object
      for (const index in defaultValue) {
        if (appConfig[key][index] === undefined) {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          appConfig[key][index] = (defaultValue as any)[index];
        }
      }
    }
  });
}