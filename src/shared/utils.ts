import { Base64 } from "js-base64";
import { SSRNode, SSNode, parseSSRLink } from "./Nodes";
export function parseSubscriptionContent(content: string) {
  const decoded = Base64.decode(content);
  const nodeList: Array<SSRNode | SSNode> = [];
  decoded.split("\n").forEach(line => {
    if (line.startsWith("ssr://")) {
      const node = parseSSRLink(line);
      node && nodeList.push(node);
    } else if (line.startsWith("ss://")) {
      // 
    }
  });
  return nodeList;
}