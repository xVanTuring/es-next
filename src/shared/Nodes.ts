/* eslint-disable @typescript-eslint/camelcase */
import { Base64 } from "js-base64";
import * as URI from "uri-js";
export interface BasicNode {
  group: string;
  id: string;
  latency: number;
  server: string;
  server_port: number;
  password: string;
  remarks: string;
  method: string;
}
export interface SSRNode extends BasicNode {
  protocol: string;
  protocolparam: string;
  obfs: string;
  obfsparam: string;
}
export enum NodeType {
  SS = "SS",
  SSR = "SSR"
}
export interface SSNode extends BasicNode {
  plugin?: string;
  plugin_opts?: string;
}

function generateID() {
  const seed = "ABCDEF01234567890";
  const arr = [];
  for (let i = 0; i < 32; i++) {
    arr.push(seed[Math.floor(Math.random() * seed.length)]);
  }
  return arr.join("");
}

export function emptySSRNode(untitled = true): SSRNode {
  return {
    remarks: untitled ? "" : "Untitled",
    server: "",
    server_port: 1080,
    password: "",
    group: "",
    id: generateID(),
    method: "",
    latency: 0,
    protocol: "",
    protocolparam: "",
    obfs: "",
    obfsparam: ""
  };
}
export function emptySSNode(untitled = true): SSNode {
  return {
    remarks: untitled ? "" : "Untitled",
    server: "",
    server_port: 1080,
    password: "",
    group: "",
    id: generateID(),
    method: "",
    latency: 0,
    plugin: "",
    plugin_opts: ""
  };
}
const ssTestKeys = ["method", "server", "server_port", "password"];
const ssrTestKeys = ["protocol", "protocolparam", "obfs", "obfsparam", ...ssTestKeys];
const featureKeys = ["remarks", "group", "id"];
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isSSRNode(node: any): node is SSRNode {
  return node != null && ssrTestKeys.every((key) => node[key] !== undefined);
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isSSNode(node: any): node is SSNode {
  return node != null && (!isSSRNode(node)) && ssTestKeys.every((key) => node[key] !== undefined);
}
const ssCheckKeys = [...ssTestKeys, "plugin", "plugin_opts"];
const configCheckKeys = ["protocol", "protocolparam", "obfs", "obfsparam", ...ssCheckKeys];
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function sameConfig(config: any, config2: any, full = false) {
  return (full ? configCheckKeys.concat(featureKeys) : configCheckKeys).every((key) => {
    return config[key as keyof SSRNode] === config2[key];
  });
}

export function toSSNode(node: SSRNode) {
  const ssNode = emptySSNode();
  (ssTestKeys.concat(featureKeys) as Array<keyof SSRNode>).forEach((key) => {
    if (node[key as keyof SSRNode] != null) {
      Object.assign(ssNode, {
        [key]: node[key as keyof SSRNode]
      });
    }
  });
  return ssNode;
}
export function toSSRNode(node: SSNode) {
  const ssrNode = emptySSRNode();
  (ssrTestKeys.concat(featureKeys) as Array<keyof SSNode>).forEach((key) => {
    if (node[key as keyof SSNode] != null) {
      Object.assign(ssrNode, {
        [key]: node[key as keyof SSNode]
      });
    }
  });
  return ssrNode;
}
export function decodeBase64(str: string) {
  return Base64.decode(str);
}
export function encodeBase64(str: string) {
  return Base64.encodeURI(str);
}
// ssr://xxxx
export function splitSSRLink(link: string) {
  const body = decodeBase64(link.substring(6));
  const _split = body.split("/?");
  const firstPart = _split[0];
  const otherPart = _split[1];
  const requiredSplit = firstPart.split(":");
  if (requiredSplit.length !== 6) {
    return null;
  }
  const otherSplit: { [x: string]: string } = {};
  otherPart && otherPart.split("&").forEach(item => {
    const params = item.split("=");
    otherSplit[params[0]] = params[1];
  });
  return {
    serverInfo: requiredSplit,
    options: otherSplit
  };
}
interface SSLinkParsedResult {
  method: string;
  password: string;
  server: string;
  port: number;
  group?: string;
  remarks?: string;
  plugin?: string;
  pluginOpts?: string;
}
export function splitSSLink(link: string) {
  let body = link.substring(5);
  const _split = body.split("#");
  body = _split[0];
  if (body.indexOf("@") === -1) {
    // https://shadowsocks.org/en/config/quick-guide.html full BASE64
    body = Base64.decode(body);
  }
  let decodedLink = `ss://${body}`;
  if (_split.length == 2) {
    decodedLink += `#${_split[1]}`;
  }
  const parsed = URI.parse(decodedLink);
  let userInfo;
  if (parsed.userinfo == null || parsed.host == null || parsed.port == null) {
    return null;
  }
  if (parsed.userinfo.indexOf(":") === -1) {
    // SIP002 base64 encoded
    userInfo = Base64.decode(parsed.userinfo).split(":");
  } else {
    userInfo = parsed.userinfo.split(":");
  }
  let port = parsed.port;
  if (typeof port === "string") {
    port = parseInt(parsed.port as string, 10);
  }
  const result: SSLinkParsedResult = {
    method: userInfo[0],
    password: userInfo[1],
    server: parsed.host,
    port: port
  };
  if (parsed.fragment) {
    result["remarks"] = decodeURIComponent(parsed.fragment);
  }
  if (parsed.query) {
    parsed.query.split("&").forEach((query) => {
      const [name, value] = query.split("=");
      switch (name) {
        case "plugin":
          {
            const pluginDecoded = decodeURIComponent(value);
            const pluginParams = pluginDecoded.split(";");
            result["plugin"] = pluginParams[0];
            result["pluginOpts"] = pluginParams.slice(1).join(";");
          }
          break;
        case "group":
          result.group = decodeBase64(value);
          break;
      }
    });
  }
  return result;
}
export function parseSSLink(link: string) {
  const parsed = splitSSLink(link);
  if (parsed == null) {
    return null;
  }
  const emptyNode = emptySSNode();
  emptyNode.server = parsed.server;
  emptyNode.server_port = parsed.port;
  emptyNode.method = parsed.method;
  emptyNode.password = parsed.password;
  if (parsed.remarks) {
    emptyNode.remarks = parsed.remarks;
  }
  if (parsed.plugin) {
    emptyNode.plugin = parsed.plugin;
  }
  if (parsed.pluginOpts) {
    emptyNode.plugin_opts = parsed.pluginOpts;
  }
  return emptyNode;
}
export function parseSSRLink(link: string): SSRNode | null {
  const emptyNode = emptySSRNode();
  const splited = splitSSRLink(link);
  if (splited !== null) {
    emptyNode.server = splited.serverInfo[0];
    emptyNode.server_port = parseInt(splited.serverInfo[1], 10);
    emptyNode.protocol = splited.serverInfo[2];
    emptyNode.method = splited.serverInfo[3];
    emptyNode.obfs = splited.serverInfo[4];
    emptyNode.password = decodeBase64(splited.serverInfo[5]);
    if (splited.options["obfsparam"]) {
      emptyNode.obfsparam = decodeBase64(splited.options["obfsparam"]);
    }
    if (splited.options["protoparam"]) {
      emptyNode.protocolparam = decodeBase64(splited.options["protoparam"]);
    }
    if (splited.options["remarks"]) {
      emptyNode.remarks = decodeBase64(splited.options["remarks"]);
    }
    if (splited.options["group"]) {
      emptyNode.group = decodeBase64(splited.options["group"]);
    }
    return emptyNode;
  }
  return null;
}
export function toSSRLink(node: SSRNode): string {
  const serverInfo = [node.server, node.server_port, node.protocol, node.method, node.obfs, encodeBase64(node.password)];
  const params = [];
  if (node.obfsparam !== "")
    params.push(`obfsparam=${encodeBase64(node.obfsparam)}`);
  if (node.protocolparam !== "")
    params.push(`protoparam=${encodeBase64(node.protocolparam)}`);
  if (node.remarks !== "")
    params.push(`remarks=${encodeBase64(node.remarks)}`);
  if (node.group !== "")
    params.push(`group=${encodeBase64(node.group)}`);
  let toBeEncode = serverInfo.join(":");
  if (params.length > 0) {
    toBeEncode = `${toBeEncode}/?${params.join("&")}`;
  }
  return `ssr://${encodeBase64(toBeEncode)}`;
}