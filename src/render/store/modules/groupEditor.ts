import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";
import { FullRootState } from "../types";

@Module({
  name: "groupEditor",
  namespaced: true
})
export default class GroupEditorModule extends VuexModule<ThisType<GroupEditorModule>, FullRootState> {
  editingTitle = "";
  originalTitle = "";
  @Mutation
  setEditingTitle(editingTitle: string) {
    this.editingTitle = editingTitle;
  }
  @Mutation
  private setOriginalTitle(originalTitle: string) {
    this.originalTitle = originalTitle;
  }
  @Action
  loadEditingTitle(name?: string) {
    const targetName = name || this.context.rootState.mainPage.selectedGroupName;
    if (targetName) {
      this.context.commit("setEditingTitle", targetName);
      this.context.commit("setOriginalTitle", targetName);
    }
  }
}