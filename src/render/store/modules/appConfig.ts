import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";
import { AppConfig, defaultConfig, ServerSubscription } from "@/shared/AppConfig";
import { BasicNode, emptySSRNode, sameConfig } from "@/shared/Nodes";
import { ipcRenderer } from "electron";
import { IpcEvents } from "@/shared/IpcEvents";
import { FullRootState } from "../types";
import { UnamedGroup } from "@/shared/consts";
@Module({
  name: "appConfig",
  namespaced: true
})
export default class AppConfigModule extends VuexModule<ThisType<AppConfigModule>, FullRootState> {
  public appConfig: AppConfig = Object.assign({}, defaultConfig);
  @Mutation
  private setEnable(enable: boolean) {
    this.appConfig.enable = enable;
  }
  @Mutation
  setActiveIndex(index: number) {
    this.appConfig.index = index;
  }
  @Mutation
  private updateAppConfig(appConfig: Partial<AppConfig>) {
    Object.assign(this.appConfig, appConfig);
  }
  @Mutation
  private appendNodeConfig(node: BasicNode) {
    this.appConfig.configs.push(node);
  }
  @Mutation
  private setNodeConfigs(configs: Array<BasicNode>) {
    this.appConfig.configs = configs;
  }
  get activeNodeId(): string {
    if (this.appConfig.index < 0 || this.appConfig.index >= this.appConfig.configs.length) {
      return "";
    }
    return this.appConfig.configs[this.appConfig.index].id;
  }
  @Action
  setActiveNodeId(payload: string) {
    let targetIndex = -1;
    for (const [index, config] of this.appConfig.configs.entries()) {
      if (config.id === payload) {
        targetIndex = index;
        break;
      }
    }
    if (this.appConfig.index !== targetIndex) {
      this.context.commit("setActiveIndex", targetIndex);
      ipcRenderer.send(IpcEvents.SYNC_APP_CONFIG, { index: this.appConfig.index });
    }
  }
  @Action
  toggleEnable() {
    this.context.dispatch("updateEnable", !this.appConfig.enable);
  }
  @Action
  updateEnable(enable: boolean) {
    if (this.appConfig.enable !== enable) {
      this.context.commit("setEnable", enable);
      ipcRenderer.send(IpcEvents.SYNC_APP_CONFIG, { enable: this.appConfig.enable });
    }
  }
  @Action
  updateNodeConfig(updatedConfig: BasicNode) {
    const clonedArr = this.appConfig.configs.slice();
    for (let index = 0; index < clonedArr.length; index++) {
      const config = clonedArr[index];
      if (config.id === updatedConfig.id) {
        if (sameConfig(config, updatedConfig, true)) {
          return;
        }
        clonedArr.splice(index, 1, Object.assign({}, updatedConfig));
        this.context.commit("setNodeConfigs", clonedArr);
        ipcRenderer.send(IpcEvents.SYNC_APP_CONFIG, { configs: this.appConfig.configs });
        break;
      }
    }
  }
  @Action
  initAppConfig(appConfig: AppConfig) {
    this.context.commit("updateAppConfig", appConfig);
  }
  @Action
  createNodeConfig() {
    const defaultSSRConfig = emptySSRNode();
    this.context.commit("appendNodeConfig", defaultSSRConfig);
    this.context.commit("mainPage/setSelectedNodeId", defaultSSRConfig.id, { root: true });
    this.context.dispatch("nodeEditor/loadEditingNode", undefined, { root: true });
    this.context.commit("page/goToNodeEditor", undefined, { root: true });
    ipcRenderer.send(IpcEvents.SYNC_APP_CONFIG, { configs: this.appConfig.configs });
  }
  @Action
  editingConfig(id?: string) {
    this.context.dispatch("nodeEditor/loadEditingNode", id, { root: true });
    this.context.commit("page/goToNodeEditor", undefined, { root: true });
  }
  @Action
  editingGroup(name?: string) {
    this.context.dispatch("groupEditor/loadEditingTitle", name, { root: true });
    this.context.commit("page/goToGroupEditor", undefined, { root: true });
  }
  @Action
  saveFromEditing() {
    const editingNode: BasicNode = this.context.rootState.nodeEditor.editingNode;
    this.context.dispatch("updateNodeConfig", editingNode);
  }
  @Action
  saveGroupFromEditing() {
    const clonedArr = this.appConfig.configs.slice();
    const originalGroupName = this.context.rootState.groupEditor.originalTitle === UnamedGroup ? "" : this.context.rootState.groupEditor.originalTitle;
    for (let index = 0; index < this.appConfig.configs.length; index++) {
      const config = this.appConfig.configs[index];
      if (config.group === originalGroupName) {
        clonedArr.splice(index, 1, Object.assign({}, config, { group: this.context.rootState.groupEditor.editingTitle }));
      }
    }
    this.context.commit("setNodeConfigs", clonedArr);
    ipcRenderer.send(IpcEvents.SYNC_APP_CONFIG, { configs: this.appConfig.configs });
  }
  /**
   * remove ss/r config with given id
   * @param id optional, if not specific, the `selectedNodeId` will be used
   */
  @Action
  removeConfig(id?: string) {
    const targetId = id || this.context.rootState.mainPage.selectedNodeId;
    if (targetId !== "") {
      const clonedArr = this.appConfig.configs.slice();
      for (let index = 0; index < clonedArr.length; index++) {
        const config = clonedArr[index];
        if (config.id === targetId) {
          clonedArr.splice(index, 1);
          this.context.commit("setNodeConfigs", clonedArr);
          ipcRenderer.send(IpcEvents.SYNC_APP_CONFIG, { configs: this.appConfig.configs });
          break;
        }
      }
    }
  }
  @Action
  removeGroup(name?: string) {
    let targetGroupName = name || this.context.rootState.mainPage.selectedGroupName;
    if (targetGroupName !== "") {
      if (targetGroupName === UnamedGroup) {
        targetGroupName = "";
      }
      const filteredConfigs = this.appConfig.configs.filter((config) => {
        return config.group !== targetGroupName;
      });
      this.context.commit("setNodeConfigs", filteredConfigs);
      ipcRenderer.send(IpcEvents.SYNC_APP_CONFIG, { configs: this.appConfig.configs });
    }
  }
  @Action
  setAppConfig({ key, value }: { key: string; value: number | string | boolean }) {
    this.context.commit("updateAppConfig", {
      [key]: value
    });
    ipcRenderer.send(IpcEvents.SYNC_APP_CONFIG, { [key]: value });
  }
  @Action
  appendSubscptionServer(payload: { item: ServerSubscription; nodes: Array<BasicNode> }) {
    const { item, nodes } = payload;
    const servers = this.appConfig.serverSubscribes.slice();
    if (servers.filter(server => server.url === item.url).length > 0) {
      return;
    }
    const configs = this.appConfig.configs.concat(nodes);
    servers.push(item);
    this.context.commit("updateAppConfig", {
      serverSubscribes: servers,
      configs: configs
    });
    ipcRenderer.send(IpcEvents.SYNC_APP_CONFIG, {
      serverSubscribes: this.appConfig.serverSubscribes,
      configs: this.appConfig.configs
    });
  }
}