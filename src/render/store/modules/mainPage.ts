import { VuexModule, Module, Mutation } from "vuex-module-decorators";
import { BasicNode } from "@/shared/Nodes";
@Module({
  namespaced: true,
  name: "mainPage"
})
export default class MainPage extends VuexModule {
    selectedNodeId = ""
    selectedGroupName = ""
    @Mutation
    setSelectedNodeId(id: string) {
      this.selectedNodeId = id;
    }
    @Mutation
    setSelectedGroupName(name: string) {
      this.selectedGroupName = name;
    }
    get selectedNodeConfig(): BasicNode | null {
      const configs: BasicNode[] = this.context.rootState.appConfig.appConfig.configs;
      for (const config of configs) {
        if (config.id === this.selectedNodeId) {
          return config;
        }
      }
      return null;
    }
}