import AppConfigModule from "./appConfig";
import PageModule from "./page";
import MainPageModule from "./mainPage";
import NodeEditerModule from "./nodeEditor";
import GroupEditorModule from "./groupEditor";
import { getModule } from "vuex-module-decorators";
import store from "../index";

export const AppConfigState = getModule(AppConfigModule, store);
export const PageState = getModule(PageModule, store);
export const MainPageState = getModule(MainPageModule, store);
export const NodeEditerState = getModule(NodeEditerModule, store);
export const GroupEditorState = getModule(GroupEditorModule, store);