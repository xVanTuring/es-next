
import { VuexModule, Module, Mutation } from "vuex-module-decorators";
export enum Page {
  Main,
  Setting,
  Intro,
  NodeEditor,
  GroupEditor
}
export enum SettingPage {
  Proxy = "Proxy",
  Common = "Common",
  SSR = "SSR",
  Subscription = "Subscription",
  Binary = "Binary",
  Shortcut = "Shortcut",
  About = "About"
}

@Module({
  namespaced: true,
  name: "page"
})
export default class PageState extends VuexModule {
  pageStatus = Page.Main;
  settingPage = SettingPage.Proxy;
  @Mutation
  goToSetting(page?: SettingPage) {
    this.pageStatus = Page.Setting;
    if (page)
      this.settingPage = page;
  }
  @Mutation
  goToMain() {
    this.pageStatus = Page.Main;
  }
  @Mutation
  goToNodeEditor() {
    this.pageStatus = Page.NodeEditor;
  }
  @Mutation
  goToGroupEditor() {
    this.pageStatus = Page.GroupEditor;
  }
}