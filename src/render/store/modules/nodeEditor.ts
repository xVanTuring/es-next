import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";
import { FullRootState } from "../types";
import { NodeType, BasicNode, emptySSRNode, isSSRNode, toSSNode, SSRNode, SSNode, toSSRNode } from "@/shared/Nodes";

@Module({
  name: "nodeEditor",
  namespaced: true
})
export default class NodeEditorModule extends VuexModule<ThisType<NodeEditorModule>, FullRootState> {
  editingType = NodeType.SSR;
  editingNode: BasicNode = emptySSRNode();
  @Mutation
  private setEditingType(editingType: NodeType) {
    this.editingType = editingType;
  }
  @Mutation
  private setEditingNode(node: BasicNode) {
    this.editingNode = node;
  }
  @Mutation
  updateEditingNode(node: Partial<BasicNode>) {
    Object.assign(this.editingNode, node);
  }
  @Action({ commit: "setEditingNode" })
  loadEditingNode(id?: string) {
    if (id != null) {
      for (const config of this.context.rootState.appConfig.appConfig.configs) {
        if (config.id === id) {
          const isSSR = isSSRNode(config);
          this.context.commit("setEditingType", isSSR ? NodeType.SSR : NodeType.SS);
          return Object.assign({}, config);
        }
      }
    }
    if (this.context.rootGetters["mainPage/selectedNodeConfig"] !== null) {
      const isSSR = isSSRNode(this.context.rootGetters["mainPage/selectedNodeConfig"]);
      this.context.commit("setEditingType", isSSR ? NodeType.SSR : NodeType.SS);
    } else {
      this.context.commit("setEditingType", NodeType.SSR);
    }
    return Object.assign({}, this.context.rootGetters["mainPage/selectedNodeConfig"] || emptySSRNode());
  }
  @Action({ commit: "setEditingType" })
  updateEditingType(editingType: NodeType) {
    switch (editingType) {
      case NodeType.SS:
        this.context.commit("setEditingNode", toSSNode(this.editingNode as SSRNode));
        break;
      case NodeType.SSR:
        this.context.commit("setEditingNode", toSSRNode(this.editingNode as SSNode));
        break;
    }
    return editingType;
  }
}