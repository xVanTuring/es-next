import { NodeType, BasicNode } from "@/shared/Nodes";
import { Page, SettingPage } from "./modules/page";
import { AppConfig } from "@/shared/AppConfig";

export interface RootState {
    meta: Meta;
    displaySubsLayer: boolean;
}
export interface FullRootState extends RootState {
    appConfig: { appConfig: AppConfig };
    mainPage: {
        selectedNodeId: string;
        selectedGroupName: string;
    };
    nodeEditor: {
        editingType: NodeType;
        editingNode: BasicNode;
    };
    groupEditor: {
        editingTitle: string;
        originalTitle: string;
    };
    page: {
        pageStatus: Page;
        settingPage: SettingPage;
    };
}
export interface Meta {
    // defaultSSRDownloadDir: string;
    version: string;
}