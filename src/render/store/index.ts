import Vue from "vue";
import Vuex, { StoreOptions } from "vuex";
import appConfig from "./modules/appConfig";
import page from "./modules/page";
import mainPage from "./modules/mainPage";
import nodeEditor from "./modules/nodeEditor";
import groupEditor from "./modules/groupEditor";
import { RootState, Meta } from "./types";
import { AppConfig } from "@/shared/AppConfig";
import { AppConfigState } from "./modules";
Vue.use(Vuex);
const store: StoreOptions<RootState> = {
  state: {
    meta: {
      version: ""
    },
    displaySubsLayer: false
  },
  mutations: {
    updateMeta(state, payload: Meta) {
      state.meta = payload;
    },
    setDisplaySubsLayer(state, value: boolean) {
      state.displaySubsLayer = value;
    }
  },
  actions: {
    initConfig(context, payload: { appConfig: AppConfig; meta: Meta }) {
      context.commit("updateMeta", payload.meta);
      AppConfigState.initAppConfig(payload.appConfig);
    }
  },
  modules: {
    appConfig,
    mainPage,
    page,
    nodeEditor,
    groupEditor
  }
};
export default new Vuex.Store<RootState>(store);
