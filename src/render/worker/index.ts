import Worker from "worker-loader!./worker";
import { SSRNode, SSNode } from "@/shared/Nodes";
const worker = new Worker();

export function fetchSubscriptionLink(link: string): Promise<{
  url: string;
  data: Array<SSRNode | SSNode> | null;
}> {
  return new Promise((resolve) => {
    worker.postMessage(link);
    worker.addEventListener("message", event => {
      const response = event.data as {
        url: string;
        data: Array<SSRNode | SSNode> | null;
      };
      if (response.url === link) {
        resolve(response);
      }
    });
  });
}
