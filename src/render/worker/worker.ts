import { parseSubscriptionContent } from "@/shared/utils";
const ctx: Worker = self as unknown as Worker;
function fetchTimeout(url: string, timeout = 5000): Promise<Response> {
  return Promise.race([
    fetch(url),
    new Promise((_, reject) =>
      setTimeout(() => reject(new Error("Fetching takes too long")), timeout)
    )
  ]) as Promise<Response>;
}
// TODO Provide cancel token
ctx.addEventListener("message", async (event) => {
  const url = event.data;
  try {
    const content = await (await fetchTimeout(url)).text();
    const parsedContent = parseSubscriptionContent(content);
    ctx.postMessage({
      url,
      data: parsedContent
    });
  } catch (error) {
    console.log(error);
    ctx.postMessage({
      url,
      data: null,
    });
  }
});