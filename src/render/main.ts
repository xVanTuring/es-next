import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import PerfectScrollbar from "vue2-perfect-scrollbar";
import "vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css";
import "./plugins/element.ts";
import { register, initConfig } from "./ipc";
Vue.config.productionTip = false;
Vue.use(PerfectScrollbar);
register();
initConfig().then(() => {
  new Vue({
    store,
    render: h => h(App)
  }).$mount("#app");
  document.title = "ES NEXT";
});
