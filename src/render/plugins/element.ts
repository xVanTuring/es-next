import Vue from "vue";
import { Popover, Form, FormItem, Select, Option, InputNumber, Input } from "element-ui";
import "./element-variables.scss";
import CollapseTransition from "element-ui/lib/transitions/collapse-transition.js";

Vue.use(Popover);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Select);
Vue.use(Option);
Vue.use(InputNumber);
Vue.use(Input);
Vue.component(CollapseTransition.name, CollapseTransition);
