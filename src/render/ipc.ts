import { ipcRenderer } from "electron";
import { IpcEvents } from "@/shared/IpcEvents";
import store from "./store";
export function register() {
  // 
}
export function initConfig() {
  ipcRenderer.send(IpcEvents.INIT_CONFIG);
  return new Promise((resolve) => {
    ipcRenderer.once(IpcEvents.INIT_CONFIG, (_, config) => {
      store.dispatch("initConfig", config);
      resolve();
    });
  });
}

export function setEnabled(enable: boolean) {
  console.log(`Setting Enable :${enable}`);
}