import events from "events";
import fse from "fs-extra";
import bootstrap from "../bootstrap";
import { cloneDeep } from "lodash-es";
import { AppConfig, defaultConfig, mergeAppConfig } from "@/shared/AppConfig";
import { appConfigFilePath } from "@/shared/paths";
import { BasicNode } from "@/shared/Nodes";
// import { initAppConfig } from '../window'
let currentConfig: AppConfig | null = null;
let _isQuiting = false;
let _isFromRenderer = false;
interface SubscribeData {
  currentConfig: AppConfig;
  changedKeys: Array<string>;
  oldConfig: AppConfig | null;
  proxyEnable: boolean;
  proxyEnabled: boolean;
}
async function read(): Promise<AppConfig> {
  const exist = await fse.pathExists(appConfigFilePath);
  if (exist) {
    try {
      return fse.readJson(appConfigFilePath);
    } catch (error) {
      // logger.error(`The config: ${appConfigPath} is corrupted, using the default config now!`)
      return Promise.resolve(defaultConfig);
    }
  } else {
    return Promise.resolve(defaultConfig);
  }
}
function isProxyStarted(appConfig: AppConfig) {
  return !!(appConfig.enable && appConfig.configs && appConfig.configs[appConfig.index]);
}
class DataCenter {
  emitter: events.EventEmitter;
  constructor() {
    this.emitter = new events.EventEmitter();
  }
  async init() {
    await bootstrap;
    const stored = await read();
    mergeAppConfig(stored);
    currentConfig = stored as AppConfig;
    this.next({
      currentConfig: stored,
      changedKeys: [],
      oldConfig: null,
      proxyEnable: isProxyStarted(stored),
      proxyEnabled: false
    });
  }
  next(data: SubscribeData) {
    this.emitter.emit("data", data);
  }
  subscribe(fn: (data: SubscribeData) => void) {
    this.emitter.on("data", fn);
  }
}

const appConfig$ = new DataCenter();


export {
  currentConfig,
  appConfig$
};
// we will make sure that every call here is valid and effective
export function updateAppConfig(targetConfig: Partial<AppConfig>, fromRenderer = false) {
  const oldConfig = cloneDeep(currentConfig) || defaultConfig;
  const updatedConfig = Object.assign({}, currentConfig, targetConfig);
  currentConfig = updatedConfig;
  _isFromRenderer = fromRenderer;
  appConfig$.next({
    currentConfig,
    changedKeys: Object.keys(targetConfig),
    oldConfig,
    proxyEnable: isProxyStarted(currentConfig),
    proxyEnabled: isProxyStarted(oldConfig)
  });
}
export function addNodeConfigs(nodeConfigs: Array<BasicNode> | BasicNode) {
  if (currentConfig) {
    updateAppConfig({ configs: currentConfig.configs.concat(Array.isArray(nodeConfigs) ? nodeConfigs : [nodeConfigs]) }, false);
  } else {
    console.error("currentConfig is null");
  }
}
export function isQuiting(target?: boolean) {
  if (target !== undefined) {
    _isQuiting = target;
  } else {
    return _isQuiting;
  }
}
appConfig$.subscribe((data) => {
  const {
    currentConfig: appConfig,
    changedKeys
  } = data;
  if (changedKeys.length !== 0) {
    // console.log("Updateing AppConfig");
    // const printVersion = Object.assign({}, appConfig);
    // printVersion.configs = [];
    // console.log(JSON.stringify(printVersion, undefined, 4));
    fse.writeJson(appConfigFilePath, appConfig, { spaces: "\t" });
    // ignore if it came from Renderer
    if (!_isFromRenderer) {
      // initAppConfig(appConfig)
    }
  }
});
