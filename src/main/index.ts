"use strict";

import { app, protocol } from "electron";
import {
  installVueDevtools
} from "vue-cli-plugin-electron-builder/lib";
import bootstrap from "./bootstrap";
import { createWindow, ensureWindow } from "./window";
import { appConfig$ } from "./controller/data";
import { registerIPC } from "./ipc";
const isDevelopment = process.env.NODE_ENV !== "production";

protocol.registerSchemesAsPrivileged([{ scheme: "app", privileges: { secure: true, standard: true } }]);


// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  ensureWindow();
});
async function main() {
  await bootstrap;
  registerIPC();
  if (isDevelopment && !process.env.IS_TEST) {
    try {
      await installVueDevtools();
    } catch (e) {
      console.error("Vue Devtools failed to install:", e.toString());
    }
  }
  createWindow();
  appConfig$.init();
}

if (isDevelopment) {
  if (process.platform === "win32") {
    process.on("message", data => {
      if (data === "graceful-exit") {
        app.quit();
      }
    });
  } else {
    process.on("SIGTERM", () => {
      app.quit();
    });
  }
}
main();