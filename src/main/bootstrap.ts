import { app } from "electron";
import fse from "fs-extra";
import { appConfigDir, appConfigFilePath, logFolder } from "@/shared/paths";
import { defaultConfig, AppConfig } from "@/shared/AppConfig";
import logger from "./logger";
export const readyPromise = new Promise((resolve) => {
  app.once("ready", () => {
    resolve();
  });
});

async function bootstrap() {
  logger.log(`appConfigDir: ${appConfigDir}`);
  await fse.ensureDir(appConfigDir);
  await fse.ensureDir(logFolder);
  const configFileExists = await fse.pathExists(appConfigFilePath);
  if (!configFileExists) {
    let config: AppConfig;
    const locale = app.getLocale();
    if (locale.startsWith("zh")) {
      config = Object.assign({}, { lang: "zh-CN" }, defaultConfig);
    } else {
      config = Object.assign({}, { lang: "en-US" }, defaultConfig);
    }
    await fse.outputJSON(appConfigFilePath, config, { spaces: 4 });
  }
  const appConfig: AppConfig = await fse.readJson(appConfigFilePath);
  // todo: fix index
  if (appConfig.enable) {
    if (appConfig.index < 0 || appConfig.index >= appConfig.configs.length) {
      appConfig.enable = false;
    }
  }
  await fse.outputJSON(appConfigFilePath, appConfig);
  await readyPromise;
}
export default bootstrap();