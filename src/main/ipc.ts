import { ipcMain, app } from "electron";
import { IpcEvents } from "@/shared/IpcEvents";
import fse from "fs-extra";
import { appConfigFilePath } from "@/shared/paths";
import { defaultConfig, AppConfig, mergeAppConfig } from "@/shared/AppConfig";
import { updateAppConfig } from "./controller/data";
export function registerIPC() {
  ipcMain.on(IpcEvents.INIT_CONFIG, async (e) => {
    let stored: AppConfig;
    try {
      stored = await fse.readJSON(appConfigFilePath);
      mergeAppConfig(stored);
    } catch (error) {
      stored = defaultConfig;
    }
    e.reply(IpcEvents.INIT_CONFIG, {
      appConfig: stored,
      meta: {
        version: app.getVersion(),
        defaultSSRDownloadDir: ""
      }
    });
  });
  ipcMain.on(IpcEvents.SYNC_APP_CONFIG, (_, appConfig: Partial<AppConfig>) => {
    updateAppConfig(appConfig, true);
    console.log(appConfig);
  });
}