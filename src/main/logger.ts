import logger from "electron-log";
import path from "path";
import { logFolder } from "@/shared/paths";

export const logPath = path.join(logFolder, "shadowsocksr-client.log");
logger.transports.file.file = logPath;
logger.transports.file.format = "{y}-{m}-{d} {h}:{i}:{s}:{ms} [{level}] {text}";
logger.transports.file.maxSize = 5 * 1024 * 1024;
logger.transports.file.level = "info";
logger.transports.console.format = "{y}-{m}-{d} {h}:{i}:{s}:{ms} [{level}] {text}";
logger.transports.console.level = process.env.NODE_ENV === "production" ? "info" : "debug";

export default logger ;