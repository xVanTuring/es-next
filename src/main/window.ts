
import { BrowserWindow } from "electron";
import {
  createProtocol,
} from "vue-cli-plugin-electron-builder/lib";
let win: BrowserWindow | null;
export function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 350, height: 500, webPreferences: {
      nodeIntegration: true,
      webSecurity: false,
    },
    autoHideMenuBar: true,
    resizable: false,
    show: true
  });
  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    win.loadURL(process.env.WEBPACK_DEV_SERVER_URL as string);
    if (!process.env.IS_TEST) win.webContents.openDevTools();
  } else {
    createProtocol("app");
    // Load the index.html when not in development
    win.loadURL("app://./index.html");
  }
  // win.webContents.once('dom-ready', () => {
  //     console.log(IpcEvents.INIT_APP_CONFIG)
  //     win!.webContents.send(IpcEvents.INIT_APP_CONFIG, 111)
  // })
  win.on("closed", () => {
    win = null;
  });
}
export function ensureWindow() {
  if (win === null) {
    createWindow();
  }
}
// export function initAppConfig(appConfig: AppConfig) {
//     if (win) {
//         // win.webContents.send(IpcEvents.INIT_CONFIG, appConfig)
//     }
// }